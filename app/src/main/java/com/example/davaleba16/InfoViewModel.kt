package com.example.davaleba16

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class InfoViewModel : ViewModel() {

    private val info = MutableLiveData<List<InfoClass>>()

    val _info: LiveData<List<InfoClass>> = info

    fun get() {

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                info.postValue(Database.db.userDao().getAll())
            }
        }

    }
    fun add(
        firstName: String,
        lastName: String,
        age: String,
        address: String,
        height: String,
    ) {

        val users = InfoClass(firstName, lastName, age, address, height)

        viewModelScope.launch {
            withContext(Dispatchers.IO){
                Database.db.userDao().addAll(users)
            }
        }


    }
}