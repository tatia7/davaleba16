package com.example.davaleba16

import android.app.Application
import android.content.Context

class AppClass : Application() {

    override fun onCreate() {
        super.onCreate()
        context = this
    }

    companion object{
        var context: Context? = null
    }
}