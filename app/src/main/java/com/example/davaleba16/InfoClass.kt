package com.example.davaleba16

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class InfoClass(
    @PrimaryKey(autoGenerate = true) val smh: Int = 0,
    @ColumnInfo(name = "first_name") val firstName: String?,
    @ColumnInfo(name = "last_name") val lastName: String?,
    @ColumnInfo(name = "age") val age: String,
    @ColumnInfo(name = "address") val address: String?,
    @ColumnInfo(name = "height") val height: String) {
    constructor(
        firstName: String?,
        lastName: String?,
        age: String,
        address: String?,
        height: String,
    ) : this(0, firstName, lastName, age, address, height)
}
