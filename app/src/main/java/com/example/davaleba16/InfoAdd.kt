package com.example.davaleba16

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface InfoAdd {
    @Query("SELECT * FROM InfoClass")
    suspend fun getAll(): List<InfoClass>

    @Query("SELECT * FROM InfoClass WHERE smh IN (:userIds)")
    suspend fun addAllByIds(userIds: IntArray): List<InfoClass>

    @Query(
        "SELECT * FROM InfoClass WHERE first_name LIKE :first AND " +
                "last_name LIKE :last LIMIT 1"
    )
    suspend fun findByName(first: String, last: String): InfoClass

    @Insert
    suspend fun addAll(vararg users: InfoClass)

    @Delete
    suspend fun remove(user: InfoClass)
}