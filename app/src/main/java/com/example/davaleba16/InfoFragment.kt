package com.example.davaleba16

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.davaleba16.databinding.FragmentInfoBinding

class InfoFragment : Fragment() {

    private lateinit var binding: FragmentInfoBinding
    private val viewModel: InfoViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInfoBinding.inflate(layoutInflater, container,false)
        init()
        return binding.root
    }

    private fun init() {

        observes()

        binding.btnInfo.setOnClickListener {
            viewModel.add(
                binding.FirstName.text.toString(),
                binding.LastName.text.toString(),
                binding.Age.text.toString(),
                binding.Address.text.toString(),
                binding.Height.text.toString()
            )
        }
    }

    private fun observes() {
        viewModel._info.observe(viewLifecycleOwner, {
            Log.d("size", "${it.size}")
        })
    }
}